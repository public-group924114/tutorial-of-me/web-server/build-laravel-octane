1. install laravel( i'm using version 9 on laravel)
2. run `composer require laravel/octane:^2.1` if any problem like a 
```
Your requirements could not be resolved to an installable set of packages.

  Problem 1
    - Root composer.json requires laravel/octane ^2.1, found laravel/octane[dev-master, 2.x-dev (alias of dev-master)] but it does not match your minimum-stability.


Installation failed, reverting ./composer.json and ./composer.lock to their original content.
```

please remove `sudo rm -rf vendor/ composer.lock` and change or add `"laravel/octane": "1.5"` on `sudo vim composer.json` like 

```
    "require": {
        "php": "^8.0.2",
        "guzzlehttp/guzzle": "^7.2",
        "laravel/framework": "^9.19",
        "laravel/sanctum": "^3.0",
        "laravel/tinker": "^2.7"
        "laravel/octane": "1.5"
    },
    "require-dev": {
        "fakerphp/faker": "^1.9.1",
        "laravel/pint": "^1.0",
        "laravel/sail": "^1.0.1",
        "mockery/mockery": "^1.4.4",
        "nunomaduro/collision": "^6.1",
        "phpunit/phpunit": "^9.5.10",
        "spatie/laravel-ignition": "^1.0"
    },
```
3. check if nginx run on server, you can run using ip addr if url returned 

```
Apache2 Default Page
```
u can kill apache using command `sudo killall apache2` and purge apache2 using command `sudo apt purge apache2`

4. run `php artisan octane:install` on directory your web
5. run `php artisan octane:start` if command returned error
```
ERROR The Swoole extension is missing.
```
run command `sudo apt-get install php8.1-dev` next run `sudo pecl install openswoole` and next install `sudo apt install php8.1-swoole`

6. installed extension php-swoole, next step run swoole using command `php artisan octane:start --server=swoole --port=80`
if any problem like a 
```
   INFO  Server running…

  Local: http://127.0.0.1:80

  Press Ctrl+C to stop the server


   Swoole\Exception

  failed to listen server port[127.0.0.1:80], Error: Permission denied[13]

  at vendor/laravel/octane/bin/createSwooleServer.php:12
      8▕         $serverState['port'] ?? '8080',
      9▕         SWOOLE_PROCESS,
     10▕         ($config['swoole']['ssl'] ?? false)
     11▕             ? SWOOLE_SOCK_TCP | SWOOLE_SSL
  ➜  12▕             : SWOOLE_SOCK_TCP,
     13▕     );
     14▕ } catch (Throwable $e) {
     15▕     Laravel\Octane\Stream::shutdown($e);
     16▕
```

you can add on your nginx config (i'm using configuration nginx on /etc/nginx/sites-available/default)

```
    location / {
         proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_set_header X-Forwarded-Proto $scheme;
         proxy_set_header X-Real-IP $remote_addr;
         proxy_set_header Host $http_host;
         proxy_pass http://127.0.0.1:8000;
    }
```

examp

```
    #ssl_protocols              TLSv1 TLSv1.1 TLSv1.2;
    #ssl_prefer_server_ciphers  on;
    #ssl_dhparam                /etc/ssl/certs/dhparam.pem;

    #ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES>    #ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-R>
    # OCSP Stapling ---
    # fetch OCSP records from URL in ssl_certificate and cache them
    #ssl_stapling         on;
    #ssl_stapling_verify  on;

    add_header Strict-Transport-Security max-age=15768000;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_pass http://127.0.0.1:8000;
    }


    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

}
```
and run using command `sudo systemctl restart nginx && sudo nginx -t && sudo php artisan octane:start --server=swoole --port=8000`
